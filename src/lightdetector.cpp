#include "lightdetector.hpp"

using namespace cv;
using namespace std;

LightDetector::LightDetector(std::shared_ptr<Config> cfg) : m_low(cfg->getHueLow(), cfg->getSatLow(), cfg->getValLow()),
                                                            m_high(cfg->getHueHigh(), cfg->getSatHigh(), cfg->getValHigh())
{
}

LightDetector::~LightDetector()
{
}

Mat LightDetector::findLightSources(const Mat & frame) const
{
    Mat buffer;
    cvtColor(frame, buffer, COLOR_BGR2HSV);
    vector<Mat> channels;
    split(buffer, channels);
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
    clahe->setClipLimit(4);
    cv::Mat dst;
    clahe->apply(channels[2], channels[2]);

#ifndef REMOVE_WINDOWS

    //        imshow("h", channels[0]);
    //        imshow("s", channels[1]);
    //        imshow("v_after", channels[2]);
#endif

    merge(channels, buffer);
    Mat thresh;
    inRange(buffer, m_low, m_high, thresh);

    morphologyEx(thresh, thresh, MORPH_OPEN, getStructuringElement(MORPH_RECT, Size(3, 3)));

#ifndef REMOVE_WINDOWS
    imshow("thresh", thresh);
#endif

    return thresh;
}
