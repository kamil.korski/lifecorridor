#include "DBSCAN.hpp"

using namespace std;
using namespace cv;

// https://en.wikipedia.org/wiki/DBSCAN
vector<vector<Point2f>> DBSCAN::Run(const vector<Point2f> & keypoints, const float eps,
        const unsigned int min_neighbouring_pts)
{
    int nr_of_keypoints = keypoints.size();
    vector<vector<Point2f>> clusters;
    vector<bool> clustered(nr_of_keypoints, false);
    vector<bool> visited(nr_of_keypoints, false);
    vector<int> neighbor_points;
    int cluster = 0;

    clusters.push_back(vector<Point2f>());

    for (int i = 0; i < nr_of_keypoints; i++)
    {
        if (!visited[i])
        {
            visited[i] = true;
            neighbor_points = RangeQuery(keypoints, keypoints.at(i), eps);
            if (neighbor_points.size() < min_neighbouring_pts)
            {
            }
            else
            {
                clusters.push_back(vector<Point2f>());
                ++cluster;
                clusters[cluster].push_back(keypoints.at(i));
                clustered[i] = true;
                for (unsigned int j = 0; j < neighbor_points.size(); j++)
                {
                    if (!visited[neighbor_points[j]])
                    {
                        vector<int> neighbor_points_tmp;
                        visited[neighbor_points[j]] = true;
                        neighbor_points_tmp = RangeQuery(keypoints, keypoints.at(neighbor_points[j]), eps);
                        if (neighbor_points_tmp.size() >= min_neighbouring_pts)
                        {
                            neighbor_points.insert(neighbor_points.end(), neighbor_points_tmp.begin(),
                                    neighbor_points_tmp.end());
                        }
                    }
                    if (!clustered[neighbor_points[j]])
                    {
                        clusters[cluster].push_back(keypoints.at(neighbor_points[j]));
                        clustered[neighbor_points[j]] = true;
                    }
                }
            }
        }
    }
    return clusters;
}

vector<int> DBSCAN::RangeQuery(const vector<Point2f> & keypoints, const Point2f & keypoint, const float eps)
{
    float distance;
    vector<int> neighbors;
    for (unsigned int i = 0; i < keypoints.size(); i++)
    {
        distance = sqrt(pow((keypoint.x - keypoints.at(i).x), 2) + pow((keypoint.y - keypoints.at(i).y), 2));
        if (distance <= eps && distance != 0.0f)
        {
            neighbors.push_back(i);
        }
    }
    return neighbors;
}
