#include <iostream>
#include <memory>

#include "config.hpp"
#include "lifecorridor.hpp"

using namespace std;

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        cout << "Too few arguments. Usage: ./Lifecorridor [config.json]" <<endl;
        return -1;
    }
    shared_ptr<Config> cfg = make_shared<Config>(argv[1]);
    if(!cfg->readConfig())
    {
        return -1;
    }

    LifeCorridor life_corridor(cfg);
    life_corridor.run();

    return 0;
}

