#include "lifecorridor.hpp"
#include "DBSCAN.hpp"

#include <iostream>
#include <numeric>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <chrono>

using namespace cv;
using namespace std;

LifeCorridor::LifeCorridor(const std::shared_ptr<Config> cfg) : m_net(cfg), m_cap(cfg->getInput()),
        m_config(cfg), m_tracker(cfg)
{

}

LifeCorridor::~LifeCorridor()
{

}

void LifeCorridor::run()
{
    Mat frame;
    while (true)
    {
        m_results.clear();

        m_cap >> frame;
        if (frame.empty())
        {
            cout << "frame empty" <<endl;
#ifndef REMOVE_WINDOWS
            waitKey();
#endif
            break;
        }
        Mat life_corridor_roi = Mat::zeros(frame.size(), CV_8UC1);


        m_net.processImg(frame);

        //        Mat frame_cpy1 = frame;
        //
        //        for(auto & box: m_net.getBoxes())
        //        {
        //            cout << "ping " <<m_net.getBoxes().size()<<endl;
        //            Mat box_img = Mat::zeros(frame.size(), CV_8UC1);
        //            rectangle(box_img,box.tl(), box.br(), 255, -1);
        //            if(countNonZero(box_img & light_morph) > 10)
        //            {
        //                rectangle(frame_cpy1, box.tl(), box.br(), Scalar(0,0,255),5);
        //                putText(frame_cpy1, "Target", box.br() - Point(box.width -5,8), 0, 0.5, Scalar(0,0,255));
        //            }
        //            else
        //            {
        //                rectangle(frame_cpy1, box.tl(), box.br(), Scalar(0,255,0), 5);
        ////                putText(frame_cpy1, "Ordinary", box.tl() - Point(0,8), 0, 1, Scalar(0,255,0));
        //
        //            }
        //
        //        }

        //        imshow("frame_cpy", frame_cpy1);

        if(m_net.getBoxes().size() > 0)
        {
            cout << "Found: " << m_net.getBoxes().size() << " cars" << endl;
            vector<Point> pt_input = m_net.getPts();
            vector<Point> corners = m_net.getCorners();
            pt_input.insert(pt_input.end(), corners.begin(), corners.end());

            Mat boxes_img = Mat::zeros(frame.size(), CV_8UC1);
            for(const auto & box: m_net.getBoxes())
            {
                rectangle(boxes_img, box.tl(), box.br(), 255, -1);
            }
            Mat canny_output;
            vector<vector<Point> > contours;
            vector<Vec4i> hierarchy;

            /// Detect edges using canny
            Canny( boxes_img, canny_output, 50, 255, 3 );
            /// Find contours
            findContours( canny_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );

            vector<Point> all_cnt;
            for(auto & cnt: contours)
            {
                all_cnt.insert(all_cnt.end(), cnt.begin(), cnt.end());
            }

            ///hugh lines

            //            Mat lines_img = Mat::zeros(frame.size(), CV_8UC1);
            //
            //            for(auto & pt : m_net.getPts())
            //            {
            //                lines_img.at<uchar>(pt) = 255;
            //            }
            //            imshow("pts", lines_img);
            //            vector<Vec4i> linesP;
            //
            //            HoughLinesP(lines_img, linesP, 10, CV_PI/180/1000, 0, 20, 1500 );

            //            cout <<"lines nr : " << linesP.size() <<endl;
            //            Mat frame_with_lines = frame;
            //            for( size_t i = 0; i < linesP.size(); i++ )
            //            {
            //                Vec4f l = linesP[i];
            //                line( frame_with_lines, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 3, LINE_AA);
            //            }


            vector<Point> data2 = findBiggestCluster(all_cnt);
//            Mat life_corridor_roi = Mat::zeros(frame.size(), CV_8UC1);

            if(data2.size() > 0)
            {

                Mat car_cluster_roi = createRoi(data2, frame.size());
                Mat car_cluster_fitline = createLifeCorridorRoi(m_net.getPts(), frame.size());


                life_corridor_roi = car_cluster_fitline & car_cluster_roi;

#ifndef REMOVE_WINDOWS
                imshow("life corridor", life_corridor_roi);
#endif

            }

//            if(m_tracker.update(frame, m_net.getBoxes(), life_corridor_roi))
//            {
//                for(const auto & obj : m_tracker.getTrackedObjList())
//                {
//                    int center_x = obj.location.tl().x + obj.location.width/2;
//                    int center_y = obj.location.tl().y + obj.location.height/2;
//                    m_results.push_back(Result(Point(center_x, center_y), obj.location.tl(), obj.location.br(), obj.is_privilaged));
//                }
//            }
        }

#ifndef REMOVE_WINDOWS
        if(waitKey(25) == 27)
        {
            break;
        }
#endif
        if(m_tracker.update(frame, m_net.getBoxes(), life_corridor_roi))
        {
            for(const auto & obj : m_tracker.getTrackedObjList())
            {
                int center_x = obj.location.tl().x + obj.location.width/2;
                int center_y = obj.location.tl().y + obj.location.height/2;
                m_results.push_back(Result(obj.id, Point(center_x, center_y), obj.location.tl(), obj.location.br(), obj.is_privilaged));
            }
        }

        publishResults();
    }

#ifndef REMOVE_WINDOWS
    waitKey();
#endif
}

vector<Rect> LifeCorridor::assingBoxesToMask(const vector<Rect> & boxes, const Mat & thresholded) const
{
    vector<Rect> result;
    for(auto & box: boxes)
    {
        Mat box_img = Mat::zeros(thresholded.size(), CV_8UC1);
        rectangle(box_img, box.tl(), box.br(), 255, -1);
        if(countNonZero(box_img & thresholded) > 10)
        {
            result.push_back(box);
#ifndef REMOVE_WINDOWS

            //            rectangle(frame_cpy1, box.tl(), box.br(), Scalar(0,0,255),5);
            //            putText(frame_cpy1, "Target", box.br() - Point(box.width -5,8), 0, 0.5, Scalar(0,0,255));
#endif
        }
#ifndef REMOVE_WINDOWS
        else
        {
            //            rectangle(frame_cpy1, box.tl(), box.br(), Scalar(0,255,0), 5);
            //            putText(frame_cpy1, "Ordinary", box.tl() - Point(0,8), 0, 1, Scalar(0,255,0));

        }
#endif
    }

    return result;
}

cv::Mat LifeCorridor::createRoi(const vector<Point> & input, const Size & img_size)
{
    vector<Point> hull;
    convexHull(input, hull);
    vector<vector<Point>> hulls;
    hulls.push_back(hull);
    vector<Point> output_approx;
    approxPolyDP(hull, output_approx, m_config->getEpsilonMultiplier() * arcLength(hull, false), false);
    vector<vector<Point>> output_approx_0;
    output_approx_0.push_back(output_approx);

    Mat result_approx = Mat::zeros(img_size, CV_8UC1);
    drawContours(result_approx, output_approx_0, 0, 255, -1);

#ifndef REMOVE_WINDOWS
    imshow("result approx", result_approx);
#endif

    return result_approx;
}

vector<Point> LifeCorridor::findBiggestCluster(vector<Point> points)
{
    vector<Point2f> input;
    Mat(points).convertTo(input, CV_32F);

    auto clusters = DBSCAN::Run(input, m_config->getDbscanDistance(), m_config->getDbscanMinPts());
    clusters.erase(remove_if(clusters.begin(), clusters.end(), [](vector<Point2f> & cl){return cl.size() == 0;}), clusters.end());

    vector<Point2f> max_cluster = *max_element(clusters.begin(), clusters.end(), [](auto & el1, auto & el2){return el1.size() < el2.size();});
    vector<Point> result;
    Mat(max_cluster).convertTo(result, CV_32FC3);

    return result;
}

Mat LifeCorridor::createLifeCorridorRoi(vector<Point> points, const Size & img_size)
{
    Vec4f  line_fit;
    fitLine(m_net.getCorners(), line_fit, DIST_L2, 0, 0.01, 0.01);


    int lefty = (-line_fit[2]*line_fit[1]/line_fit[0])+line_fit[3];
    int righty = ((img_size.width-line_fit[2])*line_fit[1]/line_fit[0])+line_fit[3];

    vector<float> distances;
    for(const auto & pt : points)
    {
        distances.push_back(abs((pt.x-img_size.width-1)*(righty-lefty) - (img_size.width-1-0)*(pt.y-righty)) /sqrt(pow(pt.x-img_size.width-1, 2) + pow(pt.y-righty, 2)));
    }

    float avg = std::accumulate(distances.begin(), distances.end(), 0)/distances.size();
    avg = avg > 2 ? avg : 2;

    Mat result_fitline = Mat::zeros(img_size, CV_8UC1);
    cv::line(result_fitline, Point(img_size.width-1,righty), Point(0,lefty), 255, avg/2);
    return result_fitline;
}

void LifeCorridor::publishResults()
{
    cout << "Results: found " << m_results.size() << " objects" << endl;
    for(const auto & obj : m_results)
    {
        cout <<"Obj id " << obj.id <<  " center: " << obj.center << " tl " << obj.top_left << " br " << obj.bottom_right << " is privileged " << obj.is_privileged << endl;
    }
    cout << "########################" << endl;
}
