#include "tracker.hpp"

using namespace cv;
using namespace std;

Tracker::Tracker(const std::shared_ptr<Config> cfg) : m_termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03),
                                                      m_light_detector(cfg), m_photo_folder(cfg->getPhotoFolder() + "/")
{

}

bool Tracker::update(const Mat & frame, const vector<Rect> boxes, const Mat & roi)
{
    if(!boxes.size() && !m_tracked_objects.size())
    {
        return false;
    }
    Mat gray;
    cvtColor(frame, gray, COLOR_BGR2GRAY);

    list<Rect> not_found_objects;
    std::copy( boxes.begin(), boxes.end(), std::back_inserter(not_found_objects));

    if(m_tracked_objects.size())
    {
        Mat light_sources = m_light_detector.findLightSources(frame);

        for(auto & obj : m_tracked_objects)
        {

            vector<uchar> status;
            vector<float> err;
            std::vector<cv::Point2f> of_features_current;

            calcOpticalFlowPyrLK(m_previous_frame, gray, obj.m_of_features_prev, of_features_current, status, err,
                    Size(m_of_window_size, m_of_window_size));

            unsigned int nr_of_points_lost = count(status.begin(), status.end(), false);

            obj.m_of_features_prev = of_features_current;

            if(nr_of_points_lost < 0.5 * of_features_current.size())
            {
                Rect tracked_obj = boundingRect(of_features_current);
                if(tracked_obj.area() >  1.2 * obj.location.area())
                {
                    //tracking error - obj increases its size too much between frames
                    obj.status = TrackedObj::LOST;
                }
                obj.location = tracked_obj;

                Mat tracked_img = Mat::zeros(frame.size(), CV_8UC1);
                rectangle(tracked_img, tracked_obj.tl(), tracked_obj.br(), 255, -1);

                if(!obj.is_privilaged)
                {
                    int points_common_with_lights = countNonZero(light_sources & tracked_img);
                    cout << "id " << obj.id <<" common with lights " << points_common_with_lights<<endl;
                    if((points_common_with_lights > 10) && (points_common_with_lights < (tracked_obj.area() * 0.3)))//todo: magic number, maybe parameter? too few common points or car blue
                    {
                        obj.is_privilaged = true;
                    }
                }

                if((countNonZero(roi) > frame.total() * 0.05) && (countNonZero(roi & tracked_img) > 0.8 * tracked_obj.area()) && !obj.on_roi)
                {
                    obj.on_roi = true;
                    if(!obj.photo_taken)
                    {
                        cout << "Taking photo of object " << obj.id << endl;
                        Mat photo = frame(tracked_obj);
                        imwrite( m_photo_folder + to_string(obj.id) + ".jpg", photo );
                    }
                }

                list<Rect>::iterator it = not_found_objects.begin();
                for(; it != not_found_objects.end(); ++it)
                {
                    Mat found_img = Mat::zeros(frame.size(), CV_8UC1);
                    rectangle(found_img, it->tl(), it->br(), 255, -1);

                    if(countNonZero(found_img & tracked_img) > (tracked_obj.area() * 0.5))
                    {
                        break;
                    }
                }

                if(it != not_found_objects.end())
                {
                    not_found_objects.erase(it);
                }
            }
            else
            {
                cout << "Object " << obj.id << " LOST" << endl;
                obj.status = TrackedObj::LOST;
            }
        }
    }

    for(const auto & not_found : not_found_objects)
    {
        TrackedObj new_obj = createNewTrackedObj(not_found, gray);
        if(new_obj.m_of_features_prev.size() > 3)
        {
            new_obj.setId(m_id++);
            m_tracked_objects.push_back(new_obj);
        }
    }

    m_tracked_objects.erase(remove_if(m_tracked_objects.begin(), m_tracked_objects.end(), [](TrackedObj & obj){return obj.status == TrackedObj::LOST;}), m_tracked_objects.end());

    gray.copyTo(m_previous_frame);

#ifndef REMOVE_WINDOWS
    Mat track_img = frame;
    for(auto & obj: m_tracked_objects)
    {
        rectangle(track_img, obj.location.tl(), obj.location.br(), Scalar(0,0,255), 2);
        putText(track_img, "id " + to_string(obj.id) + " " + to_string(obj.status) , obj.location.br() - Point(obj.location.width -5,8), 0, 0.5, Scalar(0,0,255));
    }
    imshow("track_img", track_img);
#endif

    return m_tracked_objects.size();
}

TrackedObj Tracker::createNewTrackedObj(const cv::Rect & area, const cv::Mat & gray) const
{
    TrackedObj new_obj;
    new_obj.location = area;
    new_obj.status = TrackedObj::TRACKING;
    Mat roi = Mat::zeros(gray.size(), CV_8UC1);
    rectangle(roi, area.tl(), area.br(), 255, -1);
    vector<Point2f> track_pts;
    goodFeaturesToTrack(gray, track_pts, 500, 0.01, 5, roi);
    vector<KeyPoint> kpts;
    KeyPoint::convert(track_pts, kpts);
    KeyPointsFilter::removeDuplicated(kpts);
    KeyPointsFilter::runByImageBorder(kpts, gray.size(), 10);
    KeyPointsFilter::retainBest(kpts, 20);
    KeyPoint::convert(kpts, new_obj.m_of_features_prev);

    return new_obj;
}

const list<TrackedObj> & Tracker::getTrackedObjList()
{
    return m_tracked_objects;
}

