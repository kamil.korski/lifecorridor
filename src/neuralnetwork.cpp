#include "neuralnetwork.hpp"

#include <opencv2/opencv.hpp>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace cv;
using namespace dnn;

NeuralNetwork::NeuralNetwork(const std::shared_ptr<Config> cfg) : window_name("dnn result")
{
    confThreshold = cfg->getThreshold();
    nmsThreshold = 0.4;
    scale = cfg->getNetScale();
    Scalar mean(0,0,0,0);
    swapRB = true;
    inpWidth = cfg->getNetSize();
    modelPath = cfg->getNetModel();
    configPath = cfg->getNetConfig();

    {
        std::string file = cfg->getNetClasses();
        std::ifstream ifs(file.c_str());
        if (!ifs.is_open())
            CV_Error(Error::StsError, "File " + file + " not found");
        std::string line;
        while (std::getline(ifs, line))
        {
            classes.push_back(line);
        }
    }

    net = readNet(modelPath, configPath);
    net.setPreferableBackend(0);
    net.setPreferableTarget(0);
    std::vector<std::string> outNames = net.getUnconnectedOutLayersNames();

#ifndef REMOVE_WINDOWS
    namedWindow(window_name, WINDOW_NORMAL);
#endif
}

NeuralNetwork::~NeuralNetwork()
{

}

void NeuralNetwork::processImg(const cv::Mat input)
{

    Scalar mean(0,0,0,0);

    Mat frame;
    input.copyTo(frame);

    preprocess(frame, net, Size(inpWidth, inpWidth), scale, mean, swapRB);

    std::vector<Mat> outs;
    net.forward(outs, net.getUnconnectedOutLayersNames());

    Mat result = postprocess(frame, outs, net);

#ifndef REMOVE_WINDOWS
    // Put efficiency information.
    std::vector<double> layersTimes;
    double freq = getTickFrequency() / 1000;
    double t = net.getPerfProfile(layersTimes) / freq;
    std::string label = format("Inference time: %.2f ms", t);
    putText(frame, label, Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0));

    imshow(window_name, frame);
#endif

}

void NeuralNetwork::preprocess(Mat& frame, Net& net, Size inpSize, float scale,
        const Scalar& mean, bool swapRB)
{
    Mat blob;
    if (inpSize.width <= 0) inpSize.width = frame.cols;
    if (inpSize.height <= 0) inpSize.height = frame.rows;
    blobFromImage(frame, blob, 1.0, inpSize, Scalar(), swapRB, false, CV_8U);

    net.setInput(blob, "", scale, mean);
}

Mat NeuralNetwork::postprocess(Mat& frame, const std::vector<Mat>& outs, Net& net)
{
    static std::vector<int> outLayers = net.getUnconnectedOutLayers();
    static std::string outLayerType = net.getLayer(outLayers[0])->type;

    std::vector<int> classIds;
    std::vector<float> confidences;
    std::vector<Rect> boxes;

    m_pts.clear();
    m_corners.clear();
    m_boxes.clear();

    for (size_t i = 0; i < outs.size(); ++i)
    {
        float* data = (float*)outs[i].data;

        for (int j = 0; j < outs[i].rows; ++j, data += outs[i].cols)
        {
            Mat scores = outs[i].row(j).colRange(5, outs[i].cols);
            Point classIdPoint;
            double confidence;
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
            if (confidence > confThreshold
                    && std::find(m_classes_marked.begin(), m_classes_marked.end(), classIdPoint.x) != m_classes_marked.end())
            {
                int centerX = (int)(data[0] * frame.cols);
                int centerY = (int)(data[1] * frame.rows);
                int width = (int)(data[2] * frame.cols);
                int height = (int)(data[3] * frame.rows);
                int left = centerX - width / 2;
                int top = centerY - height / 2;

                classIds.push_back(classIdPoint.x);
                confidences.push_back((float)confidence);
                boxes.push_back(Rect(left, top, width, height));
            }
        }
    }

    std::vector<int> indices;
    NMSBoxes(boxes, confidences, confThreshold, nmsThreshold, indices);
    for (size_t i = 0; i < indices.size(); ++i)
    {
        int idx = indices[i];
        Rect box = boxes[idx];
        drawPred(classIds[idx], confidences[idx], box.x, box.y,
                box.x + box.width, box.y + box.height, frame);

        m_pts.push_back(Point((box.x + box.width/2), (box.y + box.height/2)));
        m_corners.push_back(Point(box.x, box.y));
        m_corners.push_back(Point(box.x + box.width, box.y));
        m_corners.push_back(Point(box.x, box.y+box.height));
        m_corners.push_back(Point(box.x+box.width, box.y+box.height));
        m_boxes.push_back(box);
    }

    Mat result = Mat::zeros(frame.size(), CV_8UC3);
    for (auto & rec : boxes)
    {
        rectangle(result, rec.tl(), rec.br(), Scalar(255,255,255), -1);
    }

    return result;
}

void NeuralNetwork::drawPred(int classId, float conf, int left, int top, int right, int bottom, Mat& frame)
{
    rectangle(frame, Point(left, top), Point(right, bottom), Scalar(0, 255, 0));

    std::string label = format("%.2f", conf);
    if (!classes.empty())
    {
        CV_Assert(classId < (int)classes.size());
        label = classes[classId] + ": " + label;
    }

    int baseLine;
    Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);

    top = max(top, labelSize.height);
    rectangle(frame, Point(left, top - labelSize.height),
            Point(left + labelSize.width, top + baseLine), Scalar::all(255), FILLED);
    putText(frame, label, Point(left, top), FONT_HERSHEY_SIMPLEX, 0.5, Scalar());
}

