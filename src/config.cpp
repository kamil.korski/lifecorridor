#include "config.hpp"

#include <iostream>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

Config::Config(const std::string & file_path) : m_file_path(file_path)
{
}

Config::~Config()
{

}

bool Config::readConfig()
{
    try
    {
        std::cout <<"file path " <<m_file_path <<std::endl;
        pt::ptree config_file;
        pt::read_json(m_file_path, config_file);

        m_input = config_file.get<std::string>("lifecorridor.input", "");
        m_photo_folder = config_file.get<std::string>("lifecorridor.photo_folder", "./");

        std::cout <<"photos will be stored in folder: " << m_photo_folder << std::endl;

        m_net_config = config_file.get<std::string>("lifecorridor.dnn.config", "yolo/yolov3.cfg");
        m_net_model = config_file.get<std::string>("lifecorridor.dnn.model", "yolo/yolov3.weights");
        m_net_classes = config_file.get<std::string>("lifecorridor.dnn.classes", "yolo/coco.names");
        m_net_size = config_file.get<unsigned int>("lifecorridor.dnn.size", 416);
        m_scale = config_file.get<float>("lifecorridor.dnn.scale", 0.00392);
        m_threshold = config_file.get<float>("lifecorridor.dnn.thr", 0.3);
        m_epsilon_multiplier = config_file.get<double>("lifecorridor.epsilon_multiplier", m_epsilon_multiplier);

        m_dbscan_distance = config_file.get<unsigned int>("lifecorridor.dbscan.distance", m_dbscan_distance);
        m_dbscan_min_pts = config_file.get<unsigned int>("lifecorridor.dbscan.min_pts", m_dbscan_min_pts);

        m_hue_low = config_file.get<unsigned int>("lifecorridor.lightdetector.hue_low", m_hue_low);
        m_hue_high = config_file.get<unsigned int>("lifecorridor.lightdetector.hue_high", m_hue_high);
        m_sat_low = config_file.get<unsigned int>("lifecorridor.lightdetector.sat_low", m_sat_low);
        m_sat_high = config_file.get<unsigned int>("lifecorridor.lightdetector.sat_high", m_sat_high);
        m_val_low = config_file.get<unsigned int>("lifecorridor.lightdetector.val_low", m_val_low);
        m_val_high = config_file.get<unsigned int>("lifecorridor.lightdetector.val_high", m_val_high);
    }
    catch(const pt::json_parser_error & error)
    {
        std::cout <<"Could not read file" << std::endl;
        return false;
    }

    return true;
}

std::string Config::getInput() const
{
    return m_input;
}

std::string Config::getNetClasses() const
{
    return m_net_classes;
}

std::string Config::getNetConfig() const
{
    return m_net_config;
}

std::string Config::getNetModel() const
{
    return m_net_model;
}

unsigned int Config::getNetSize() const
{
    return m_net_size;
}

float Config::getNetScale() const
{
    return m_scale;
}

float Config::getThreshold() const
{
    return m_threshold;
}

unsigned int Config::getDbscanDistance() const
{
    return m_dbscan_distance;
}

unsigned int Config::getDbscanMinPts() const
{
    return m_dbscan_min_pts;
}

double Config::getEpsilonMultiplier() const
{
    return m_epsilon_multiplier;
}

unsigned int Config::getHueHigh() const
{
    return m_hue_high;
}

unsigned int Config::getHueLow() const
{
    return m_hue_low;
}

unsigned int Config::getSatHigh() const
{
    return m_sat_high;
}

unsigned int Config::getSatLow() const
{
    return m_sat_low;
}

unsigned int Config::getValHigh() const
{
    return m_val_high;
}

unsigned int Config::getValLow() const
{
    return m_val_low;
}

const std::string& Config::getPhotoFolder() const
{
    return m_photo_folder;
}
