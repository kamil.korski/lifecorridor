Parameters in config file

"input" - source of video, for example 
"input" : "/dev/video0",

"photo_folder" - folder where car photos will be stored. Car photos are named
afer their tracking id. Folder needs to be created before run.

"dbscan" : "distance" - max distance between two cars to be assigned to the same
cluster. Important: the smaller the cars on video the larger it should be.
In config file set to 800 which is good for sparse car presence. Can be lowered
to ~200 when expecting a lot of cars. 

dnn size : set to 320 for better speed, can cange to 416 for better accuracy