#ifndef INCLUDE_LIGHTDETECTOR_HPP_
#define INCLUDE_LIGHTDETECTOR_HPP_

#include "config.hpp"
#include <memory>
#include <opencv2/opencv.hpp>


class LightDetector
{
public:
    LightDetector(std::shared_ptr<Config> cfg);
    ~LightDetector();
    cv::Mat findLightSources(const cv::Mat & frame) const;
private:
    cv::Scalar m_low;
    cv::Scalar m_high;
};



#endif /* INCLUDE_LIGHTDETECTOR_HPP_ */
