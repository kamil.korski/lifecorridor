#ifndef INCLUDE_TRACKER_HPP_
#define INCLUDE_TRACKER_HPP_

#include <memory>
#include "config.hpp"
#include <opencv2/opencv.hpp>
#include "lightdetector.hpp"

struct TrackedObj
{
    TrackedObj() : id(0), status(TRACKING) {}
    ~TrackedObj() {}
    void setId(unsigned int i) {id = i;}
    unsigned int id;
    enum eStatus {TRACKING, LOST};
    cv::Rect location;
    bool is_privilaged = false;
    std::vector<cv::Point2f> m_of_features_prev;
    eStatus status;
    bool on_roi = false;
    bool photo_taken = false;
};

class Tracker
{
public:
    Tracker(const std::shared_ptr<Config> cfg);
    bool update(const cv::Mat & frame, const std::vector<cv::Rect> boxes, const cv::Mat & roi);
    const std::list<TrackedObj> & getTrackedObjList();
private:
    TrackedObj createNewTrackedObj(const cv::Rect & area, const cv::Mat & gray) const;
    std::list<TrackedObj> m_tracked_objects;
    cv::TermCriteria m_termcrit;
    cv::Mat m_previous_frame;
    const unsigned int m_of_window_size { 31 };
    unsigned int m_id = 0;
    LightDetector m_light_detector;
    std::string m_photo_folder;
};




#endif /* INCLUDE_TRACKER_HPP_ */
