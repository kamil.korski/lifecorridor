#ifndef INCLUDE_LIFECORRIDOR_HPP_
#define INCLUDE_LIFECORRIDOR_HPP_

#include <memory>

#include <opencv2/opencv.hpp>

#include "neuralnetwork.hpp"
#include "config.hpp"
#include "tracker.hpp"
#include "result.hpp"

class LifeCorridor
{
public:
    LifeCorridor(const std::shared_ptr<Config> cfg);
    ~LifeCorridor();
    void run();
private:
    std::vector<cv::Rect> assingBoxesToMask(const std::vector<cv::Rect> & boxes, const cv::Mat & thresholded) const;
    cv::Mat createRoi(const std::vector<cv::Point> & input, const cv::Size & img_size);
    std::vector<cv::Point> findBiggestCluster(std::vector<cv::Point> points);
    cv::Mat createLifeCorridorRoi(std::vector<cv::Point> points, const cv::Size & img_size);
    void publishResults();

//    enum eMode {SEARCHING, TRACKING_ROI};todo:unused, delete?
    NeuralNetwork m_net;
    cv::VideoCapture m_cap;
    const std::shared_ptr<Config> m_config;
    Tracker m_tracker;
    std::vector<Result> m_results;
};



#endif /* INCLUDE_LIFECORRIDOR_HPP_ */
