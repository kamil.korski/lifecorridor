#ifndef INCLUDE_CONFIG_HPP_
#define INCLUDE_CONFIG_HPP_

#include <boost/property_tree/ptree.hpp>
#include <string>

class Config
{
public:
    Config(const std::string & file_path);
    ~Config();

    bool readConfig();
    std::string getInput() const;
    std::string getNetClasses() const;
    std::string getNetConfig() const;
    std::string getNetModel() const;
    unsigned int getNetSize() const;
    float getNetScale() const;
    float getThreshold() const;
    unsigned int getDbscanDistance() const;
    unsigned int getDbscanMinPts() const;
    double getEpsilonMultiplier() const;
    unsigned int getHueHigh() const;
    unsigned int getHueLow() const;
    unsigned int getSatHigh() const;
    unsigned int getSatLow() const;
    unsigned int getValHigh() const;
    unsigned int getValLow() const;
    const std::string& getPhotoFolder() const;

private:
    std::string m_file_path;
    std::string m_net_config;
    std::string m_net_model;
    std::string m_net_classes;
    unsigned int m_net_size = 0;
    float m_scale = 0;
    float m_threshold = 0;
    std::string m_input;
    std::string m_photo_folder;
    double m_epsilon_multiplier = 0.035;

    unsigned int m_dbscan_distance = 70;
    unsigned int m_dbscan_min_pts = 10;

    unsigned int m_hue_low = 100;
    unsigned int m_hue_high = 140;
    unsigned int m_sat_low = 80;
    unsigned int m_sat_high = 255;
    unsigned int m_val_low = 40;
    unsigned int m_val_high = 255;
};


#endif /* INCLUDE_CONFIG_HPP_ */
