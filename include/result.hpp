#ifndef INCLUDE_RESULT_HPP_
#define INCLUDE_RESULT_HPP_

#include <opencv2/opencv.hpp>

struct Result
{
    Result(unsigned int i, cv::Point cnt, cv::Point tl, cv::Point br, bool priv) : id(i), center(cnt), top_left(tl),
                                                                   bottom_right(br), is_privileged(priv){};
    unsigned int id;
    cv::Point center;
    cv::Point top_left;
    cv::Point bottom_right;
    bool is_privileged;
};

#endif /* INCLUDE_RESULT_HPP_ */
