#ifndef INCLUDE_NEURALNETWORK_HPP_
#define INCLUDE_NEURALNETWORK_HPP_

#include <vector>
#include <string>

#include <opencv2/dnn.hpp>
#include <opencv2/imgproc.hpp>

#include "config.hpp"

class NeuralNetwork
{
public:
    NeuralNetwork(const std::shared_ptr<Config> cfg);
    ~NeuralNetwork();
    void processImg(const cv::Mat input);

    std::vector<cv::Point> getPts() {return m_pts;}
    std::vector<cv::Point> getCorners() {return m_corners;}
    std::vector<cv::Rect> getBoxes() {return m_boxes;}

private:
    void preprocess(cv::Mat& frame, cv::dnn::Net& net, cv::Size inpSize, float scale, const cv::Scalar& mean, bool swapRB);
    cv::Mat postprocess(cv::Mat& frame, const std::vector<cv::Mat>& out, cv::dnn::Net& net);
    void drawPred(int classId, float conf, int left, int top, int right, int bottom, cv::Mat& frame);

    float confThreshold;
    float nmsThreshold;
    float scale;
    std::vector<std::string> classes;
    bool swapRB;
    int inpWidth;
    std::string modelPath;
    std::string configPath;
    std::string window_name;

    cv::dnn::Net net;
    std::vector<int> m_classes_marked = {2, 3, 5, 7};

    std::vector<cv::Point> m_pts;
    std::vector<cv::Point> m_corners;
    std::vector<cv::Rect> m_boxes;

};

#endif /* INCLUDE_NEURALNETWORK_HPP_ */
