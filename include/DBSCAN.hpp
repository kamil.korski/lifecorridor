#ifndef TOOLS_TARGETDETECTION_INCLUDE_DBSCAN_HPP_
#define TOOLS_TARGETDETECTION_INCLUDE_DBSCAN_HPP_

#include <iostream>
#include <opencv2/opencv.hpp>

class DBSCAN
{
public:
    DBSCAN()
    {
    };
    ~DBSCAN()
    {
    };
    static std::vector<std::vector<cv::Point2f>> Run(const std::vector<cv::Point2f> & keypoints, const float eps,
            const unsigned int min_neighbouring_pts);
private:
    static std::vector<int> RangeQuery(const std::vector<cv::Point2f> & keypoints, const cv::Point2f & keypoint, const float eps);
};

#endif /* TOOLS_TARGETDETECTION_INCLUDE_DBSCAN_HPP_ */
